FROM php:7.1

RUN set -x \
    && apt-get update \
    && apt-get install -y \
		lftp \
		zip \
		unzip \
	&& docker-php-ext-install pdo_mysql

# Install Composer

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
